import 'bootstrap'


const Navbar: React.FC = () => {
  return (
    <>
      <div id="page" className="hfeed site">
        <a className="skip-link screen-reader-text" href="#content">
          Saltar al contenido
        </a>
        <div id="header-section" className="h-on-top no-transparent">
          <header
            id="masthead"
            className="site-header header-contained is-sticky no-scroll no-t h-on-top"
            role="banner"
          >
            <div className="container">
              <div className="site-branding">
                <div className="site-brand-inner has-logo-img no-desc">
                  <div className="site-logo-div">
                    <a
                      href="index.html"
                      className="custom-logo-link no-t-logo"
                      rel="home"
                    //   itemProp="url"
                    >
                      <img
                        width="199"
                        height="73"
                        src="wp-content/uploads/2021/02/logos-KHS-1.html"
                        className="custom-logo"
                        alt="Know How Services"
                        decoding="async"
                        loading="lazy"
                        // itemProp="logo"
                      />
                    </a>
                  </div>
                </div>
              </div>
              <div className="header-right-wrapper">
                <a href="#0" id="nav-toggle">
                  Menú<span></span>
                </a>
                <nav
                  id="site-navigation"
                  className="main-navigation"
                  role="navigation"
                >
                  <ul className="onepress-menu">
                    <li
                      id="menu-item-1208"
                      className="menu-item menu-item-type-custom menu-item-object-custom menu-item-1208"
                    >
                      <a href="#inicio">Inicio</a>
                    </li>
                    <li
                      id="menu-item-27"
                      className="menu-item menu-item-type-custom menu-item-object-custom menu-item-27"
                    >
                      <a href="#nosotros">Nosotros</a>
                    </li>
                    <li
                      id="menu-item-28"
                      className="menu-item menu-item-type-custom menu-item-object-custom menu-item-28"
                    >
                      <a href="#servicios">Servicios</a>
                    </li>
                    <li
                      id="menu-item-32"
                      className="menu-item menu-item-type-custom menu-item-object-custom menu-item-32"
                    >
                      <a href="#contacto">Contacto</a>
                    </li>
                    <li
                      id="menu-item-1465"
                      className="menu-item menu-item-type-custom menu-item-object-custom menu-item-1465"
                    >
                      <a href="pagos/index.html">Pagos en Linea</a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </header>

          <section
            id="inicio"
            data-images='["https:\/\/www.kyhservices.com\/wp-content\/uploads\/2017\/11\/onepress2_hero.jpg","https:\/\/www.kyhservices.com\/wp-content\/uploads\/2016\/02\/business1.jpg"]'
            className="hero-slideshow-wrapper hero-slideshow-normal"
          >
            <div className="slider-spinner">
              <div className="double-bounce1"></div>
              <div className="double-bounce2"></div>
            </div>

            <div className="container" style={{}}>
              <div className="hero__content hero-content-style1">
                <h2 className="hero-large-text">
                  Somos
                  <span className="js-rotating">
                    Eficientes | Responsables | Adaptables | Perfección
                  </span>
                </h2>
                <div className="hero-small-text">
                  <p>
                    Generamos Atención integral e igualitaria a todos los
                    clientes, gestión y control eficaz de todos los servicios,
                    fidelización de clientes, velamos por la mejora continua de
                    nuestros procesos.
                  </p>
                </div>
                <a
                  href="index.html#acerca%20de"
                  className="btn btn-secondary-outline btn-lg"
                >
                  Sobre nosotros
                </a>
                <a
                  href="index.html#contacto"
                  className="btn btn-secondary-outline btn-lg"
                >
                  Contactenos
                </a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </>
  );
};

export default Navbar;
